package com.yota.example.dao.mappers;

import com.yota.example.dao.domain.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {

    void save(Order order);

    List<Order> findAll();
}
