package com.yota.example.dao.mappers;

import com.yota.example.dao.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {

    List<User> findAll();

    void save(User user);

    User findByName(@Param("username") String username);

    void saveAll(@Param("users") List<User> users);
}
