package com.yota.example.dao.mappers;

import com.yota.example.dao.domain.Report;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ReportMapper {
    List<Report> findAll();
}
