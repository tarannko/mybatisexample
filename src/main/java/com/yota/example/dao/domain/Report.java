package com.yota.example.dao.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Report {
    private String position;
    private Integer sum;
    private Integer countUser;
}
