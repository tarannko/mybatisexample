package com.yota.example.dao.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Order {
    private Long id;
    @NonNull
    private String position;
    @NonNull
    private Integer sum;

    //Вариант А
    @NonNull
    private User user;

    //Вариант Б
//    @NonNull
//    private Integer userId;
}
