package com.yota.example.dao.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    @NonNull
    private String name;
    @NonNull
    private String password;
    private Date created;
    private List<Order> orderList;
}
