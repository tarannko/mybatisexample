CREATE TABLE users
(
    id            SERIAL PRIMARY KEY NOT NULL,
    user_name     TEXT               NOT NULL,
    user_password TEXT               NOT NULL,
    created       TIMESTAMP          NOT NULL
);

CREATE TABLE orders
(
    id       SERIAL PRIMARY KEY            NOT NULL,
    position TEXT                          NOT NULL,
    sum      INTEGER                       NOT NULL,
    user_id  INTEGER REFERENCES users (id) NOT NULL
)
