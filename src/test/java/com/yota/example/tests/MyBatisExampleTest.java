package com.yota.example.tests;

import com.yota.example.dao.domain.Order;
import com.yota.example.dao.domain.Report;
import com.yota.example.dao.domain.User;
import com.yota.example.dao.mappers.OrderMapper;
import com.yota.example.dao.mappers.ReportMapper;
import com.yota.example.dao.mappers.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyBatisExampleTest {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ReportMapper reportMapper;

    @Test
    public void saveUserTest() {
        User user = new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        userMapper.save(user);

    }

    @Test
    public void findAllUsersTest() {
        List<User> all = userMapper.findAll();
        System.out.println(all);
    }

    @Test
    public void findAllOrdersTest() {
        List<Order> all = orderMapper.findAll();
        System.out.println(all);
    }

    @Test
    public void saveAllUsersTest() {
        List<User> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add(new User(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        }

        userMapper.saveAll(list);
    }

    @Test
    public void saveOrderTest() {
        User user = new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        userMapper.save(user);
        orderMapper.save(new Order("EUR/USD", 100, user));
    }

    @Test
    public void findAllReports() {
        List<Report> all = reportMapper.findAll();
        System.out.println(all);
    }
}